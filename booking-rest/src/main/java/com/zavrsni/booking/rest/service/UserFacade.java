package com.zavrsni.booking.rest.service;

import com.zavrsni.booking.rest.domain.dto.UserDto;
import com.zavrsni.booking.rest.domain.form.UserForm;

/**
 * Created by mrados on 01.03.19.
 */
public interface UserFacade {
    UserDto saveUser(UserForm userForm);
}
