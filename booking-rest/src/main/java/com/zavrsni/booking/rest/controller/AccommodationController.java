package com.zavrsni.booking.rest.controller;

import com.zavrsni.booking.model.Accommodation;
import com.zavrsni.booking.rest.domain.dto.AccommodationDto;
import com.zavrsni.booking.rest.domain.dto.ReviewDto;
import com.zavrsni.booking.rest.domain.dto.UserDto;
import com.zavrsni.booking.rest.domain.form.AccommodationForm;
import com.zavrsni.booking.rest.domain.form.ReviewForm;
import com.zavrsni.booking.rest.domain.form.UserForm;
import com.zavrsni.booking.rest.service.AccommodationFacade;
import com.zavrsni.booking.rest.service.ReviewFacade;
import com.zavrsni.booking.rest.service.UserFacade;
import com.zavrsni.booking.rest.validators.AccommodationFormValidator;
import com.zavrsni.booking.rest.validators.ReviewFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

/**
 * Created by mrados on 01.03.19.
 */
@RestController
@RequestMapping(value = "/v1/accommodation")
public class AccommodationController {

    @Autowired
    AccommodationFacade accommodationFacade;

    @Autowired
    ReviewFacade reviewFacade;

    @Autowired
    AccommodationFormValidator accommodationFormValidator;

    @Autowired
    ReviewFormValidator reviewFormValidator;

    @GetMapping
    public ResponseEntity<Accommodation> getAccommodationById(@RequestParam Long id) {
        Accommodation accommodation = accommodationFacade.getAccommodationById(id);
        return new ResponseEntity<>(accommodation, HttpStatus.OK);
    }

    @PostMapping(value = "/create")
    public ResponseEntity<AccommodationDto> createAccommodation(@Valid @RequestBody AccommodationForm accommodationForm, HttpServletRequest request) {
        accommodationForm.setUsername(request.getUserPrincipal().getName());
        return new ResponseEntity<>(accommodationFacade.createAccommodation(accommodationForm), HttpStatus.OK);
    }

    @GetMapping(value = "/all")
    public ResponseEntity<List<AccommodationDto>> getAll() {
        return new ResponseEntity<>(accommodationFacade.getAllAccommodations(), HttpStatus.OK);
    }

    @GetMapping(value = "/pageable")
    public ResponseEntity<Page<AccommodationDto>> getAccommodationByCity(
            @RequestParam(required = false) String city,
            @RequestParam int page,
            @RequestParam int pageSize
    ) {
        return new ResponseEntity<>(accommodationFacade.getAccommodationPageable(page, pageSize, city), HttpStatus.OK);
    }


    @PostMapping(value = "/review/create")
    public ResponseEntity<ReviewDto> reviewAccommodation(@Valid @RequestBody ReviewForm reviewForm, HttpServletRequest httpServletRequest) {
        reviewForm.setUsername(httpServletRequest.getUserPrincipal().getName());

        return new ResponseEntity<>(reviewFacade.saveReview(reviewForm), HttpStatus.CREATED);
    }


    @InitBinder("accommodationForm")
    private void initAccommodationFormValidator(WebDataBinder binder) {
        binder.setValidator(accommodationFormValidator);
    }

    @InitBinder("reviewForm")
    private void initReviewFormValidator(WebDataBinder binder) {
        binder.setValidator(reviewFormValidator);
    }
}
