package com.zavrsni.booking.rest.validators;

import com.zavrsni.booking.rest.BookingRestErrorCodes;
import com.zavrsni.booking.rest.domain.form.ReviewForm;
import com.zavrsni.booking.service.AccommodationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by mrados on 07.03.19.
 */
@Component
public class ReviewFormValidator implements Validator {

    @Autowired
    AccommodationService accommodationService;

    @Override
    public boolean supports(Class<?> clazz) {
        return ReviewForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ReviewForm reviewForm = (ReviewForm) target;

        validateAccommodation(reviewForm, errors);
        validateVote(reviewForm, errors);
    }


    private void validateAccommodation(ReviewForm reviewForm, Errors errors) {
        if (reviewForm.getAccommodationId() == null) {
            errors.rejectValue("accommodationId", BookingRestErrorCodes.ACCOMMODATION_ID_NULL, "Accommodation id can't be null.");
        }
        else if (!accommodationService.existsById(reviewForm.getAccommodationId())) {
            errors.rejectValue("accommodationId", BookingRestErrorCodes.ACCOMMODATION_DOES_NOT_EXIST, "Accommodation does not exist.");
        }
    }

    private void validateVote(ReviewForm reviewForm, Errors errors) {
        if (reviewForm.getVote() == null) {
            errors.rejectValue("vote", BookingRestErrorCodes.ACCOMMODATION_VOTE_NULL, "Accommodation vote can't be null.");

        }
        else if (reviewForm.getVote() <= 0 || reviewForm.getVote() > 5) {
            errors.rejectValue("vote", BookingRestErrorCodes.ACCOMMODATION_VOTE_INVALID, "Accommodation vote invalid. Must be [1-5].");
        }
    }
}
