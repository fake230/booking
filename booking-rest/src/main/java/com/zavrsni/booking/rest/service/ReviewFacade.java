package com.zavrsni.booking.rest.service;

import com.zavrsni.booking.rest.domain.dto.ReviewDto;
import com.zavrsni.booking.rest.domain.form.ReviewForm;

public interface ReviewFacade {
    ReviewDto saveReview(ReviewForm reviewForm);
}
