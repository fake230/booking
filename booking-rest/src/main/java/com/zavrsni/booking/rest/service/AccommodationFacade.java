package com.zavrsni.booking.rest.service;

import com.zavrsni.booking.model.Accommodation;
import com.zavrsni.booking.rest.domain.dto.AccommodationDto;
import com.zavrsni.booking.rest.domain.dto.UserDto;
import com.zavrsni.booking.rest.domain.form.AccommodationForm;
import com.zavrsni.booking.rest.domain.form.UserForm;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by mrados on 01.03.19.
 */
public interface AccommodationFacade {
    Accommodation getAccommodationById(Long id);

    AccommodationDto createAccommodation(AccommodationForm accommodationForm);

    List<AccommodationDto> getAllAccommodations();

    Page<AccommodationDto> getAccommodationPageable(int page, int pageSize, String city);
}
