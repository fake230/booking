package com.zavrsni.booking.rest.domain.dto;

import com.zavrsni.booking.model.Accommodation;
import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

/**
 * Created by mrados on 03/04/2019
 */
public class AccommodationPriceDto {

    private Long id;
    private int numberOfPersons;
    private Double price;


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("numberOfPersons", numberOfPersons)
                .append("price", price)
                .toString();
    }


    // --------------- get / set methods ------------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
