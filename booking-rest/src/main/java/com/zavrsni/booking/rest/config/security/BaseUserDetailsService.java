package com.zavrsni.booking.rest.config.security;

import com.zavrsni.booking.model.Users.User;
import com.zavrsni.booking.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Primary
@Service
public class BaseUserDetailsService implements UserDetailsService {
    private static final Logger LOG = LoggerFactory.getLogger(BaseUserDetailsService.class);

    @Autowired
    UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            LOG.info("User not found with username={}", username);
            throw new UsernameNotFoundException(username);
        }

        return new org.springframework.security.core.userdetails.User(
                username, user.getPassword(), user.isEnabled(), true, true, true, user.getRoles()
        );
    }
}
