package com.zavrsni.booking.rest.domain.form;

import java.io.Serializable;

/**
 * Created by mrados on 07.03.19.
 */
public class ReviewForm implements Serializable {
    private Long accommodationId;
    private Double vote;
    private String comment;
    private String username;


    public Long getAccommodationId() {
        return accommodationId;
    }

    public void setAccommodationId(Long accommodationId) {
        this.accommodationId = accommodationId;
    }

    public Double getVote() {
        return vote;
    }

    public void setVote(Double vote) {
        this.vote = vote;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String userId) {
        this.username = userId;
    }
}
