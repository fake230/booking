package com.zavrsni.booking.rest.domain.dto;

/**
 * Created by mrados on 05.03.19.
 */
public class BaseError {

    private String code;
    private String message;


    public BaseError() {
    }

    public BaseError(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
