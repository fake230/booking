package com.zavrsni.booking.rest.validators;

import com.zavrsni.booking.rest.BookingRestErrorCodes;
import com.zavrsni.booking.rest.domain.form.AccommodationForm;
import com.zavrsni.booking.service.AccommodationTypeService;
import com.zavrsni.booking.service.AmenityService;
import com.zavrsni.booking.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

/**
 * Created by mrados on 07.03.19.
 */
@Component
public class AccommodationFormValidator implements Validator {


    @Autowired
    AccommodationTypeService accommodationTypeService;

    @Autowired
    AmenityService amenityService;

    @Autowired
    CityService cityService;


    @Override
    public boolean supports(Class<?> clazz) {
        return AccommodationForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        AccommodationForm accommodationForm = (AccommodationForm) target;

        validateName(accommodationForm, errors);
        validateDescription(accommodationForm, errors);
        validateAddress(accommodationForm, errors);
        validateAccommodationType(accommodationForm, errors);
        validateAmenities(accommodationForm, errors);
        validateCity(accommodationForm, errors);
        validatePrices(accommodationForm, errors);
    }


    private void validateName(AccommodationForm accommodationForm, Errors errors) {
        if (!StringUtils.hasText(accommodationForm.getName())) {
            errors.rejectValue("name", BookingRestErrorCodes.ACCOMMODATION_NAME_EMPTY, "Accommodation name can't be empty.");
        }
    }


    private void validateDescription(AccommodationForm accommodationForm, Errors errors) {
        if (!StringUtils.hasText(accommodationForm.getDescription())) {
            errors.rejectValue("description", BookingRestErrorCodes.ACCOMMODATION_DESC_EMPTY, "Accommodation description can't be empty.");
        }
    }

    private void validateAddress(AccommodationForm accommodationForm, Errors errors) {
        if (!StringUtils.hasText(accommodationForm.getAddress())) {
            errors.rejectValue("description", BookingRestErrorCodes.ACCOMMODATION_ADDRESS_EMPTY, "Accommodation address can't be empty.");
        }
    }

    private void validateAccommodationType(AccommodationForm accommodationForm, Errors errors) {
        if (accommodationForm.getAccommodationTypeId() == null) {
            errors.rejectValue("accommodationTypeId", BookingRestErrorCodes.ACCOMMODATION_TYPE_EMPTY, "Accommodation type can't be empty.");
        }
        else if (!accommodationTypeService.existsById(accommodationForm.getAccommodationTypeId())) {
            errors.rejectValue("accommodationTypeId", BookingRestErrorCodes.ACCOMMODATION_TYPE_DOES_NOT_EXIST, "Accommodation type does not exist.");
        }
    }

    private void validateAmenities(AccommodationForm accommodationForm, Errors errors) {
        if (accommodationForm.getAmenitiesIds() == null) {
            errors.rejectValue("amenitiesIds", BookingRestErrorCodes.ACCOMMODATION_AMENITIES_NULL, "Accommodation amenities can't be null.");
        }
        else {
            accommodationForm.getAmenitiesIds().forEach(id -> {
                if (!amenityService.existsById(id)) {
                    errors.rejectValue("amenitiesIds", BookingRestErrorCodes.ACCOMMODATION_AMENITY_DOES_NOT_EXIST, "Accommodation amenity with id={" + id + "} does not exist");
                }
            });
        }
    }

    private void validateCity(AccommodationForm accommodationForm, Errors errors) {
        if (accommodationForm.getCityId() == null) {
            errors.rejectValue("cityId", BookingRestErrorCodes.ACCOMMODATION_CITY_NULL, "Accommodation city can't be null.");
        }
        else if (!cityService.existsById(accommodationForm.getCityId())) {
            errors.rejectValue("cityId", BookingRestErrorCodes.ACCOMMODATION_CITY_DOES_NOT_EXIST, "Accommodation city does not exist.");
        }
    }

    private void validatePrices(AccommodationForm accommodationForm, Errors errors) {
        if (accommodationForm.getPrices() == null) {
            errors.rejectValue("prices", BookingRestErrorCodes.ACCOMMODATION_PRICES_NULL, "Accommodation prices can't be null.");

        }
        else if (accommodationForm.getPrices().size() <= 0) {
            errors.rejectValue("prices", BookingRestErrorCodes.ACCOMMODATION_PRICES_EMPTY, "Accommodation prices can't be empty.");
        }
    }

}
