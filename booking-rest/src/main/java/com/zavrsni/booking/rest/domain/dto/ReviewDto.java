package com.zavrsni.booking.rest.domain.dto;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Created by mrados on 03/04/2019
 */
public class ReviewDto {
    private Long id;
    private Long accommodationId;
    private UserDto user;
    private String comment;
    private Double vote;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("accommodationId", accommodationId)
                .append("user", user)
                .append("comment", comment)
                .append("vote", vote)
                .toString();
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getAccommodationId() {
        return accommodationId;
    }

    public void setAccommodationId(Long accommodationId) {
        this.accommodationId = accommodationId;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Double getVote() {
        return vote;
    }

    public void setVote(Double vote) {
        this.vote = vote;
    }
}
