package com.zavrsni.booking.rest.controller;

import com.zavrsni.booking.rest.domain.dto.UserDto;
import com.zavrsni.booking.rest.domain.form.UserForm;
import com.zavrsni.booking.rest.service.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by mrados on 01.03.19.
 */
@RestController
@RequestMapping(value = "/v1/user")
public class UserController {

    @Autowired
    UserFacade userFacade;

    @PostMapping(value = "/register")
    public ResponseEntity<UserDto> userRegistration(@RequestBody UserForm userForm) {
        return new ResponseEntity<>(userFacade.saveUser(userForm), HttpStatus.OK);
    }
}
