package com.zavrsni.booking.rest.domain.dto;

import com.zavrsni.booking.model.AmenityType;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.io.Serializable;

/**
 * Created by mrados on 03/02/2019
 */
public class AmenityDto {

    private Long id;
    private String name;
    private String amenityType;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("amenityType", amenityType)
                .toString();
    }

    // ------------- get / set methods ----------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAmenityType() {
        return amenityType;
    }

    public void setAmenityType(String amenityType) {
        this.amenityType = amenityType;
    }
}
