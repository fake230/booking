package com.zavrsni.booking.rest.service.impl;

import com.zavrsni.booking.mapper.AccommodationDtoMapper;
import com.zavrsni.booking.mapper.form.AccommodationFormMapper;
import com.zavrsni.booking.model.Accommodation;
import com.zavrsni.booking.rest.domain.dto.AccommodationDto;
import com.zavrsni.booking.rest.domain.form.AccommodationForm;
import com.zavrsni.booking.rest.service.AccommodationFacade;
import com.zavrsni.booking.service.AccommodationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by mrados on 01.03.19.
 */
@Service
public class AccommodationFacadeImpl implements AccommodationFacade {

    @Autowired
    AccommodationService accommodationService;

    @Autowired
    AccommodationFormMapper accommodationFormMapper;

    @Autowired
    AccommodationDtoMapper accommodationDtoMapper;

    @Override
    public Accommodation getAccommodationById(Long id) {
        return accommodationService.getAccommodationById(id);
    }

    @Override
    public AccommodationDto createAccommodation(AccommodationForm accommodationForm) {
        Accommodation accommodation = accommodationFormMapper.mapToEntity(accommodationForm, null);
        Accommodation savedAccommodation = accommodationService.saveAccommodation(accommodation);

        return accommodationDtoMapper.mapFromEntity(savedAccommodation, null);
    }

    @Override
    public List<AccommodationDto> getAllAccommodations() {
        List<Accommodation> accommodationList = accommodationService.getAllAccommodations();
        return accommodationList.stream().map(a -> {
            return accommodationDtoMapper.mapFromEntity(a, null);
        }).collect(Collectors.toList());
    }

    @Override
    public Page<AccommodationDto> getAccommodationPageable(int page, int pageSize, String city) {
        Pageable p = PageRequest.of(page, pageSize);
        Page<Accommodation> accommodationList = accommodationService.getAllAccommodationsPageable(city, p);

        System.out.println(accommodationList);

        return accommodationList.map(a -> {
            return accommodationDtoMapper.mapFromEntity(a, null);
        });
    }
}
