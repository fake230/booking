package com.zavrsni.booking.rest.domain.dto;

import com.zavrsni.booking.model.AccommodationType;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.time.LocalDate;
import java.util.List;

/**
 * Created by mrados on 03/02/2019
 */
public class AccommodationDto  {

    private Long id;
    private String name;
    private String description;
    private String address;
    private CityDto city;
    private AccommodationType accommodationType;
    private List<LocalDate> reservedDates;
    private List<AmenityDto> amenities;
    private UserDto owner;
    private List<ReviewDto> reviews;
    private Double avgRating;
    private List<AccommodationPriceDto> accommodationPrices;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("description", description)
                .append("address", address)
                .append("city", city)
                .append("accommodationType", accommodationType)
                .append("reservedDates", reservedDates)
                .append("amenities", amenities)
                .append("owner", owner)
                .append("reviews", reviews)
                .append("avgRating", avgRating)
                .append("accommodationPrices", accommodationPrices)
                .toString();
    }

    // ----------- get / set methods ---------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public CityDto getCity() {
        return city;
    }

    public void setCity(CityDto city) {
        this.city = city;
    }

    public AccommodationType getAccommodationType() {
        return accommodationType;
    }

    public void setAccommodationType(AccommodationType accommodationType) {
        this.accommodationType = accommodationType;
    }

    public List<LocalDate> getReservedDates() {
        return reservedDates;
    }

    public void setReservedDates(List<LocalDate> reservedDates) {
        this.reservedDates = reservedDates;
    }

    public List<AmenityDto> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<AmenityDto> amenities) {
        this.amenities = amenities;
    }

    public UserDto getOwner() {
        return owner;
    }

    public void setOwner(UserDto owner) {
        this.owner = owner;
    }

    public List<ReviewDto> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewDto> reviews) {
        this.reviews = reviews;
    }

    public Double getAvgRating() {
        return avgRating;
    }

    public void setAvgRating(Double avgRating) {
        this.avgRating = avgRating;
    }

    public List<AccommodationPriceDto> getAccommodationPrices() {
        return accommodationPrices;
    }

    public void setAccommodationPrices(List<AccommodationPriceDto> accommodationPrices) {
        this.accommodationPrices = accommodationPrices;
    }
}
