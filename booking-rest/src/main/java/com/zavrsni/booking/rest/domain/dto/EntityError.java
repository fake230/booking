package com.zavrsni.booking.rest.domain.dto;

/**
 * Created by mrados on 05.03.19.
 */
public class EntityError extends BaseError {

    private String entityId;


    public EntityError() {

    }

    public EntityError(final String code, final String message, final String entityId) {
        super(code, message);
        this.entityId = entityId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }
}
