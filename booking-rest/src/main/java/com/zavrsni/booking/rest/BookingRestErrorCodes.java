package com.zavrsni.booking.rest;

/**
 * Created by mrados on 07.03.19.
 */
public class BookingRestErrorCodes {

    public static final String ACCOMMODATION_NAME_EMPTY = "acc.name.empty";
    public static final String ACCOMMODATION_DESC_EMPTY = "acc.description.empty";
    public static final String ACCOMMODATION_ADDRESS_EMPTY = "acc.address.empty";
    public static final String ACCOMMODATION_TYPE_EMPTY = "acc.type.empty";
    public static final String ACCOMMODATION_TYPE_DOES_NOT_EXIST = "acc.type.does.not.exists";
    public static final String ACCOMMODATION_AMENITIES_NULL = "acc.amenites.null";
    public static final String ACCOMMODATION_AMENITY_DOES_NOT_EXIST = "acc.amenity.does.not.exist";
    public static final String ACCOMMODATION_CITY_NULL = "acc.city.null";
    public static final String ACCOMMODATION_CITY_DOES_NOT_EXIST = "acc.city.does.not.exist";
    public static final String ACCOMMODATION_PRICES_EMPTY = "acc.prices.empty";
    public static final String ACCOMMODATION_PRICES_NULL = "acc.prices.null";

    public static final String ACCOMMODATION_ID_NULL = "acc.id.null";
    public static final String ACCOMMODATION_DOES_NOT_EXIST = "acc.does.not.exist";
    public static final String ACCOMMODATION_VOTE_INVALID = "acc.vote.invalid";
    public static final String ACCOMMODATION_VOTE_NULL = "acc.vote.null";
}
