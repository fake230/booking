package com.zavrsni.booking.rest.service.impl;

import com.zavrsni.booking.mapper.ReviewDtoMapper;
import com.zavrsni.booking.mapper.form.ReviewFormMapper;
import com.zavrsni.booking.model.Review;
import com.zavrsni.booking.rest.domain.dto.ReviewDto;
import com.zavrsni.booking.rest.domain.form.ReviewForm;
import com.zavrsni.booking.rest.service.ReviewFacade;
import com.zavrsni.booking.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mrados on 07.03.19.
 */
@Service
public class ReviewFacadeImpl implements ReviewFacade {

    @Autowired
    ReviewFormMapper reviewFormMapper;

    @Autowired
    ReviewService reviewService;

    @Autowired
    ReviewDtoMapper reviewDtoMapper;

    @Override
    public ReviewDto saveReview(ReviewForm reviewForm) {
        Review review = reviewFormMapper.mapToEntity(reviewForm, null);
        return reviewDtoMapper.mapFromEntity(reviewService.saveReview(review), null);
    }
}
