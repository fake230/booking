package com.zavrsni.booking.rest.service.impl;

import com.zavrsni.booking.mapper.UserDtoMapper;
import com.zavrsni.booking.mapper.form.UserFormMapper;
import com.zavrsni.booking.model.Users.User;
import com.zavrsni.booking.rest.domain.dto.UserDto;
import com.zavrsni.booking.rest.domain.form.UserForm;
import com.zavrsni.booking.rest.service.UserFacade;
import com.zavrsni.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mrados on 01.03.19.
 */
@Service
public class UserFacadeImpl implements UserFacade {

    @Autowired
    UserService userService;

    @Autowired
    UserFormMapper userFormMapper;

    @Autowired
    UserDtoMapper userDtoMapper;

    @Override
    public UserDto saveUser(UserForm userForm) {
        User user = userFormMapper.mapToEntity(userForm, null);
        return userDtoMapper.mapFromEntity(userService.saveUser(user), null);
    }
}
