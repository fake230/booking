package com.zavrsni.booking.rest.domain.form;

import com.zavrsni.booking.model.AccommodationPrice;

import java.io.Serializable;
import java.util.List;

/**
 * Created by mrados on 03/02/2019
 */
public class AccommodationForm implements Serializable {

    private String name;
    private String description;
    private String address;
    private Long accommodationTypeId;
    private List<Long> amenitiesIds;
    private Long cityId;
    private String username;
    private List<AccommodationPrice> prices;


    // ------------ get / set methods -----------------------------------------------
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Long getAccommodationTypeId() {
        return accommodationTypeId;
    }

    public void setAccommodationTypeId(Long accommodationTypeId) {
        this.accommodationTypeId = accommodationTypeId;
    }

    public List<Long> getAmenitiesIds() {
        return amenitiesIds;
    }

    public void setAmenitiesIds(List<Long> amenitiesIds) {
        this.amenitiesIds = amenitiesIds;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public List<AccommodationPrice> getPrices() {
        return prices;
    }

    public void setPrices(List<AccommodationPrice> prices) {
        this.prices = prices;
    }
}
