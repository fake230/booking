package com.zavrsni.booking.mapper.form;

import com.zavrsni.booking.mapper.EntityMapper;
import com.zavrsni.booking.model.Users.User;
import com.zavrsni.booking.rest.domain.form.UserForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mrados on 01.03.19.
 */
@Component
public class UserFormMapper implements EntityMapper<User, UserForm> {

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public UserForm mapFromEntity(User entity, Locale locale) {
        throw new UnsupportedOperationException("Method not implemented!");
    }

    @Override
    public UserForm mapFromEntity(User entity, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException("Method not implemented!");
    }

    @Override
    public User mapToEntity(UserForm source, Locale locale) {
        User user = new User();
        user.setFirstName(source.getFirstName());
        user.setLastName(source.getLastName());
        user.setEmail(source.getEmail());
        user.setPassword(passwordEncoder.encode(source.getPassword()));
        user.setUsername(source.getUsername());

        // enable user on registration
        user.setEnabled(true);
        return user;
    }

    @Override
    public User mapToEntity(UserForm source, Locale locale, Map<String, Object> config) {
        return null;
    }
}
