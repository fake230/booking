package com.zavrsni.booking.mapper;

import com.zavrsni.booking.model.Review;
import com.zavrsni.booking.rest.domain.dto.ReviewDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mrados on 03/04/2019
 */
@Component
public class ReviewDtoMapper implements EntityMapper<Review, ReviewDto> {

    @Autowired
    UserDtoMapper userDtoMapper;

    @Override
    public ReviewDto mapFromEntity(Review entity, Locale locale) {
        ReviewDto reviewDto = new ReviewDto();

        reviewDto.setId(entity.getId());
        reviewDto.setAccommodationId(entity.getAccommodation().getId());
        reviewDto.setComment(entity.getComment());
        reviewDto.setVote(entity.getVote());
        reviewDto.setUser(userDtoMapper.mapFromEntity(entity.getUser(), null));

        return reviewDto;
    }

    @Override
    public ReviewDto mapFromEntity(Review entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public Review mapToEntity(ReviewDto source, Locale locale) {
        return null;
    }

    @Override
    public Review mapToEntity(ReviewDto source, Locale locale, Map<String, Object> config) {
        return null;
    }
}
