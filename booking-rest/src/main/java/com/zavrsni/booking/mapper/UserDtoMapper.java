package com.zavrsni.booking.mapper;


import com.zavrsni.booking.model.Users.User;
import com.zavrsni.booking.rest.domain.dto.UserDto;
import org.mapstruct.Mapper;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mrados on 01.03.19.
 */
 @Component
public class UserDtoMapper implements EntityMapper<User, UserDto> {

    @Override
    public UserDto mapFromEntity(User entity, Locale locale) {
        UserDto userDto = new UserDto();
        userDto.setId(entity.getId());
        userDto.setFirstName(entity.getFirstName());
        userDto.setLastName(entity.getLastName());
        userDto.setEmail(entity.getEmail());
        userDto.setUsername(entity.getUsername());

        return userDto;
    }

    @Override
    public UserDto mapFromEntity(User entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public User mapToEntity(UserDto source, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public User mapToEntity(UserDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }
}
