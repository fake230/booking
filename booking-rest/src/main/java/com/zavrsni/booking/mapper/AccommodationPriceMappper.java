package com.zavrsni.booking.mapper;

import com.zavrsni.booking.model.AccommodationPrice;
import com.zavrsni.booking.rest.domain.dto.AccommodationPriceDto;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mrados on 03/04/2019
 */
@Component
public class AccommodationPriceMappper implements EntityMapper<AccommodationPrice, AccommodationPriceDto> {

    @Override
    public AccommodationPriceDto mapFromEntity(AccommodationPrice entity, Locale locale) {
        AccommodationPriceDto accommodationPriceDto = new AccommodationPriceDto();

        accommodationPriceDto.setId(entity.getId());
        accommodationPriceDto.setNumberOfPersons(entity.getNumberOfPersons());
        accommodationPriceDto.setPrice(entity.getPrice());

        return accommodationPriceDto;
    }

    @Override
    public AccommodationPriceDto mapFromEntity(AccommodationPrice entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public AccommodationPrice mapToEntity(AccommodationPriceDto source, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public AccommodationPrice mapToEntity(AccommodationPriceDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }
}
