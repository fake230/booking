package com.zavrsni.booking.mapper;

import com.zavrsni.booking.model.City;
import com.zavrsni.booking.rest.domain.dto.CityDto;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mrados on 03/02/2019
 */
@Component
public class CityDtoMapper implements EntityMapper<City, CityDto> {
    @Override
    public CityDto mapFromEntity(City entity, Locale locale) {
        CityDto cityDto = new CityDto();
        cityDto.setId(entity.getId());
        cityDto.setLat(entity.getLat());
        cityDto.setLng(entity.getLng());
        cityDto.setName(entity.getName());

        return cityDto;
    }

    @Override
    public CityDto mapFromEntity(City entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public City mapToEntity(CityDto source, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public City mapToEntity(CityDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }
}
