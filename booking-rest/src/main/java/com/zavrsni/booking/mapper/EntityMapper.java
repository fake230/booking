package com.zavrsni.booking.mapper;

import java.util.Locale;
import java.util.Map;

public interface EntityMapper <E, S> {
    S mapFromEntity(E entity, Locale locale);

    S mapFromEntity(E entity, Locale locale, Map<String, Object> config);

    E mapToEntity(S source, Locale locale);

    E mapToEntity(S source, Locale locale, Map<String, Object> config);

    default Class getEntityClass() {
        throw new UnsupportedOperationException("Method getEntityClass() not defined. Mapper interface implementing EntityMapper must provide its own implementation that returns the class of Entity this mapper is designed to support.");
    }
}
