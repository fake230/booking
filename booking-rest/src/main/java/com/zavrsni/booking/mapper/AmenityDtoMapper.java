package com.zavrsni.booking.mapper;

import com.zavrsni.booking.model.Amenity;
import com.zavrsni.booking.rest.domain.dto.AmenityDto;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mrados on 03/02/2019
 */
@Component
public class AmenityDtoMapper implements EntityMapper<Amenity, AmenityDto> {
    @Override
    public AmenityDto mapFromEntity(Amenity entity, Locale locale) {
        AmenityDto amenityDto = new AmenityDto();
        amenityDto.setId(entity.getId());
        amenityDto.setName(entity.getName());
        amenityDto.setAmenityType(entity.getAmenityType().getName());

        return amenityDto;
    }

    @Override
    public AmenityDto mapFromEntity(Amenity entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public Amenity mapToEntity(AmenityDto source, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Amenity mapToEntity(AmenityDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }
}
