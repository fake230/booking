package com.zavrsni.booking.mapper.form;

import com.zavrsni.booking.mapper.EntityMapper;
import com.zavrsni.booking.model.Accommodation;
import com.zavrsni.booking.model.AccommodationPrice;
import com.zavrsni.booking.model.Amenity;
import com.zavrsni.booking.model.Users.User;
import com.zavrsni.booking.repository.AccommodationTypeRepository;
import com.zavrsni.booking.rest.domain.dto.AccommodationDto;
import com.zavrsni.booking.rest.domain.form.AccommodationForm;
import com.zavrsni.booking.rest.domain.form.UserForm;
import com.zavrsni.booking.service.AccommodationTypeService;
import com.zavrsni.booking.service.AmenityService;
import com.zavrsni.booking.service.CityService;
import com.zavrsni.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by mrados on 01.03.19.
 */
@Component
public class AccommodationFormMapper implements EntityMapper<Accommodation, AccommodationForm> {

    @Autowired
    AccommodationTypeService accommodationTypeService;

    @Autowired
    CityService cityService;

    @Autowired
    AmenityService amenityService;

    @Autowired
    UserService userService;

    @Override
    public AccommodationForm mapFromEntity(Accommodation entity, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public AccommodationForm mapFromEntity(Accommodation entity, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Accommodation mapToEntity(AccommodationForm form, Locale locale) {
        Accommodation accommodation = new Accommodation();
        accommodation.setName(form.getName());
        accommodation.setDescription(form.getDescription());
        accommodation.setAddress(form.getAddress());
        accommodation.setAccommodationType(accommodationTypeService.getAccommodationTypeById(form.getAccommodationTypeId()));
        accommodation.setCity(cityService.getCityById(form.getCityId()));

        List<Amenity> amenities =
                form.getAmenitiesIds()
                .stream()
                .map(id -> amenityService.getAmenityById(id)).collect(Collectors.toList());

        accommodation.setAmenities(amenities);
        accommodation.setReservedDates(new ArrayList<>());
        accommodation.setOwner(userService.getUserByUsername(form.getUsername()));
        form.getPrices().forEach(p -> p.setAccommodation(accommodation));

        accommodation.setAccommodationPrice(form.getPrices());
        accommodation.setReviews(new ArrayList<>());
        return accommodation;
    }

    @Override
    public Accommodation mapToEntity(AccommodationForm source, Locale locale, Map<String, Object> config) {
        return null;
    }
}
