package com.zavrsni.booking.mapper.form;

import com.zavrsni.booking.mapper.EntityMapper;
import com.zavrsni.booking.model.Review;
import com.zavrsni.booking.model.Users.User;
import com.zavrsni.booking.rest.domain.form.ReviewForm;
import com.zavrsni.booking.service.AccommodationService;
import com.zavrsni.booking.service.ReviewService;
import com.zavrsni.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;

/**
 * Created by mrados on 07.03.19.
 */
@Component
public class ReviewFormMapper implements EntityMapper<Review, ReviewForm> {

    @Autowired
    AccommodationService accommodationService;

    @Autowired
    UserService userService;

    @Autowired
    ReviewService reviewService;

    @Override
    public ReviewForm mapFromEntity(Review entity, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public ReviewForm mapFromEntity(Review entity, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Review mapToEntity(ReviewForm source, Locale locale) {
        Review review = new Review();
        User user = userService.getUserByUsername(source.getUsername());

        if (reviewService.existsByAccommodationAndUser(source.getAccommodationId(), user.getId())) {
            review = reviewService.getByAccommodationAndUser(source.getAccommodationId(), user.getId());
        }

        review.setAccommodation(accommodationService.getAccommodationById(source.getAccommodationId()));
        review.setUser(userService.getUserByUsername(source.getUsername()));
        review.setComment(source.getComment());
        review.setVote(source.getVote());

        return review;
    }

    @Override
    public Review mapToEntity(ReviewForm source, Locale locale, Map<String, Object> config) {
        return null;
    }
}
