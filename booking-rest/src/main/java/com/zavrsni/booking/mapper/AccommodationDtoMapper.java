package com.zavrsni.booking.mapper;

import com.zavrsni.booking.model.Accommodation;
import com.zavrsni.booking.model.Review;
import com.zavrsni.booking.rest.domain.dto.AccommodationDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by mrados on 03/02/2019
 */
@Component
public class AccommodationDtoMapper implements EntityMapper<Accommodation, AccommodationDto> {
    @Autowired
    AmenityDtoMapper amenityDtoMapper;

    @Autowired
    CityDtoMapper cityDtoMapper;

    @Autowired
    UserDtoMapper userDtoMapper;

    @Autowired
    ReviewDtoMapper reviewDtoMapper;

    @Autowired
    AccommodationPriceMappper accommodationPriceMappper;

    @Override
    public AccommodationDto mapFromEntity(Accommodation entity, Locale locale) {
        AccommodationDto accommodationDto = new AccommodationDto();

        accommodationDto.setId(entity.getId());
        accommodationDto.setName(entity.getName());
        accommodationDto.setDescription(entity.getDescription());
        accommodationDto.setAccommodationType(entity.getAccommodationType());
        accommodationDto.setAddress(entity.getAddress());
        accommodationDto.setAmenities(entity.getAmenities().stream().map(a -> amenityDtoMapper.mapFromEntity(a, null)).collect(Collectors.toList()));
        accommodationDto.setReservedDates(entity.getReservedDates());
        accommodationDto.setCity(cityDtoMapper.mapFromEntity(entity.getCity(), null));
        accommodationDto.setOwner(userDtoMapper.mapFromEntity(entity.getOwner(), null));
        accommodationDto.setReviews(entity.getReviews().stream().map(r -> reviewDtoMapper.mapFromEntity(r, null)).collect(Collectors.toList()));
        accommodationDto.setAccommodationPrices(entity.getAccommodationPrice().stream().map(p -> accommodationPriceMappper.mapFromEntity(p, null)).collect(Collectors.toList()));

        if (entity.getReviews().size() > 0){
            Double allVotes = entity.getReviews().stream().mapToDouble(Review::getVote).sum();
            Double avgRating = allVotes / entity.getReviews().size();

            accommodationDto.setAvgRating(avgRating);
        }
        else
        {
            accommodationDto.setAvgRating(0.);
        }

        return accommodationDto;
    }

    @Override
    public AccommodationDto mapFromEntity(Accommodation entity, Locale locale, Map<String, Object> config) {
        return null;
    }

    @Override
    public Accommodation mapToEntity(AccommodationDto source, Locale locale) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Accommodation mapToEntity(AccommodationDto source, Locale locale, Map<String, Object> config) {
        throw new UnsupportedOperationException();
    }
}
