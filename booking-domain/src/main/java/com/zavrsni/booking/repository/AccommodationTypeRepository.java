package com.zavrsni.booking.repository;

import com.zavrsni.booking.model.AccommodationType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by mrados on 03/02/2019
 */
@Repository
public interface AccommodationTypeRepository extends JpaRepository<AccommodationType, Long> {
}
