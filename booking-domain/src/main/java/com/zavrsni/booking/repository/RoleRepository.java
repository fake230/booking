package com.zavrsni.booking.repository;

import com.zavrsni.booking.model.Users.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
    Role getById(Integer id);
    Role getRoleByName(String name);
}
