package com.zavrsni.booking.repository;

import com.zavrsni.booking.model.Review;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mrados on 07.03.19.
 */
public interface ReviewRepository extends JpaRepository<Review, Long> {
    boolean existsByAccommodation_IdAndUserId(Long accommodationId, Long userId);
    Review getByAccommodationIdAndUserId(Long accommodationId, Long userId);
}
