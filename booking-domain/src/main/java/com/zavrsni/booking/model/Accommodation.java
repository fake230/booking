package com.zavrsni.booking.model;

import com.zavrsni.booking.model.Users.User;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;

/**
 * Created by mrados on 03/02/2019
 */
@Entity
@Table(name = "ACCOMMODATION")
public class Accommodation implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @Column(name = "NAME")
    private String name;

    @Column(name = "DESCRIPTION")
    private String description;

    @Column(name  = "ADDRESS")
    private String address;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CITY_ID", referencedColumnName = "ID")
    private City city;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "TYPE_ID", referencedColumnName = "ID")
    private AccommodationType accommodationType;


    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "RESERVED_DATES", joinColumns = @JoinColumn(name = "ACCOMMODATION_ID"))
    @Column(name = "DATE")
    private List<LocalDate> reservedDates;


    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "ACCOMMODATION_AMENITY",
            joinColumns = @JoinColumn(name = "ACCOMMODATION_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "AMENITY_ID", referencedColumnName = "ID")
    )
    @Fetch(FetchMode.SELECT)
    private List<Amenity> amenities;


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "OWNER_ID", referencedColumnName = "ID")
    private User owner;


    @OneToMany(cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name = "ACCOMMODATION_ID")
    private List<Review> reviews;

    @OneToMany(cascade = CascadeType.ALL,
            orphanRemoval = true)
    @JoinColumn(name = "ACCOMMODATION_ID")
    private List<AccommodationPrice> accommodationPrice;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("name", name)
                .append("description", description)
                .append("address", address)
                .append("city", city)
                .append("accommodationType", accommodationType)
                .append("reservedDates", reservedDates)
                .append("amenities", amenities)
                .append("owner", owner)
                .append("reviews", reviews)
                .append("accommodationPrices", accommodationPrice)
                .toString();
    }

    // -------- get / set methods -----------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public AccommodationType getAccommodationType() {
        return accommodationType;
    }

    public void setAccommodationType(AccommodationType accommodationType) {
        this.accommodationType = accommodationType;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<LocalDate> getReservedDates() {
        return reservedDates;
    }

    public void setReservedDates(List<LocalDate> reservedDates) {
        this.reservedDates = reservedDates;
    }

    public List<Amenity> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Amenity> amenities) {
        this.amenities = amenities;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<Review> getReviews() {
        return reviews;
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public List<AccommodationPrice> getAccommodationPrice() {
        return accommodationPrice;
    }

    public void setAccommodationPrice(List<AccommodationPrice> accommodationPrice) {
        this.accommodationPrice = accommodationPrice;
    }
}
