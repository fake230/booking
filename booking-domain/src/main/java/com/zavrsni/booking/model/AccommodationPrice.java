package com.zavrsni.booking.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import javax.persistence.*;

/**
 * Created by mrados on 03/04/2019
 */
@Entity
@Table(name = "ACCOMMODATION_PRICE")
public class AccommodationPrice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "ACCOMMODATION_ID", referencedColumnName = "ID")
    private Accommodation accommodation;

    @Column(name = "NUMBER_OF_PERSONS")
    private int numberOfPersons;

    @Column(name = "PRICE")
    private Double price;


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("accommodation", accommodation)
                .append("numberOfPersons", numberOfPersons)
                .append("price", price)
                .toString();
    }

    // ------------ get / set methods --------------------------------------
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getNumberOfPersons() {
        return numberOfPersons;
    }

    public void setNumberOfPersons(int numberOfPersons) {
        this.numberOfPersons = numberOfPersons;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Accommodation getAccommodation() {
        return accommodation;
    }

    public void setAccommodation(Accommodation accommodation) {
        this.accommodation = accommodation;
    }
}
