package com.zavrsni.booking.service;


import com.zavrsni.booking.model.City;

public interface CityService  {
    City getCityById(Long id);
    boolean existsById(Long id);
}
