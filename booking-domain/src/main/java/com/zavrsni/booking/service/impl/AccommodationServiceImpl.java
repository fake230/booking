package com.zavrsni.booking.service.impl;

import com.zavrsni.booking.model.Accommodation;
import com.zavrsni.booking.repository.AccommodationRepository;
import com.zavrsni.booking.service.AccommodationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by mrados on 03/02/2019
 */
@Service
public class AccommodationServiceImpl implements AccommodationService {

    @Autowired
    AccommodationRepository accommodationRepository;

    @Override
    public Accommodation getAccommodationById(Long id) {
        return accommodationRepository.getOne(id);
    }

    @Override
    public Accommodation saveAccommodation(Accommodation accommodation) {
       return accommodationRepository.save(accommodation);
    }

    @Override
    public boolean existsById(Long id) {
        return accommodationRepository.existsById(id);
    }

    @Override
    public List<Accommodation> getAllAccommodations() {
        return accommodationRepository.findAll();
    }

    @Override
    public Page<Accommodation> getAllAccommodationsPageable(String city, Pageable p) {
        if (city != null) {
            return accommodationRepository.findAllByCityName(city, p);
        }
        else return null;
    }
}
