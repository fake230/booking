package com.zavrsni.booking.service.impl;

import com.zavrsni.booking.model.Users.Role;
import com.zavrsni.booking.model.Users.User;
import com.zavrsni.booking.repository.RoleRepository;
import com.zavrsni.booking.repository.UserRepository;
import com.zavrsni.booking.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by mrados on 01.03.19.
 */
@Service
public class UserServiceImpl implements UserService {

    final String USER_ROLE = "ROLE_USER";

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Override
    public User saveUser(User user) {
        // set default role to USER_ROLE
        Set<Role> roles = new HashSet<>();
        roles.add(roleRepository.getRoleByName(USER_ROLE));

        user.setRoles(roles);
        return userRepository.save(user);
    }

    @Override
    public User getUserByUsername(String username) {
        return userRepository.findByUsername(username);
    }
}
