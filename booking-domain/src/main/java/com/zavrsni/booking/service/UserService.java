package com.zavrsni.booking.service;

import com.zavrsni.booking.model.Users.User;

public interface UserService {
    User saveUser(User user);
    User getUserByUsername(String username);
}
