package com.zavrsni.booking.service;

import com.zavrsni.booking.model.Accommodation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

/**
 * Created by mrados on 03/02/2019
 */
public interface AccommodationService {
    Accommodation getAccommodationById(Long id);
    Accommodation saveAccommodation(Accommodation accommodation);

    boolean existsById(Long id);

    List<Accommodation> getAllAccommodations();


    Page<Accommodation> getAllAccommodationsPageable(String city, Pageable pageable);
}
