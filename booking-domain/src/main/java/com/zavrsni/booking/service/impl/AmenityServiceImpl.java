package com.zavrsni.booking.service.impl;

import com.zavrsni.booking.model.Amenity;
import com.zavrsni.booking.repository.AmenityRepository;
import com.zavrsni.booking.service.AmenityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by mrados on 03/02/2019
 */
@Service
public class AmenityServiceImpl implements AmenityService {

    @Autowired
    AmenityRepository amenityRepository;

    @Override
    public Amenity getAmenityById(Long id) {
        return amenityRepository.getOne(id);
    }

    @Override
    public boolean existsById(Long id) {
        return amenityRepository.existsById(id);
    }
}
