package com.zavrsni.booking.service.impl;

import com.zavrsni.booking.model.AccommodationType;
import com.zavrsni.booking.repository.AccommodationTypeRepository;
import com.zavrsni.booking.service.AccommodationTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by mrados on 03/02/2019
 */
@Service
public class AccommodationTypeServiceImpl implements AccommodationTypeService {

    @Autowired
    AccommodationTypeRepository accommodationTypeRepository;

    @Override
    public AccommodationType getAccommodationTypeById(Long id) {
        return accommodationTypeRepository.getOne(id);
    }

    @Override
    public boolean existsById(Long id) {
        return accommodationTypeRepository.existsById(id);
    }
}
