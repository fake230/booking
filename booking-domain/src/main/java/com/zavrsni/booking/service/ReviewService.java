package com.zavrsni.booking.service;

import com.zavrsni.booking.model.Review;

public interface ReviewService {
    Review saveReview(Review review);
    boolean existsByAccommodationAndUser(Long accommodationId, Long userId);
    Review getByAccommodationAndUser(Long accommodationId, Long userId);
    Review getById(Long id);
}
