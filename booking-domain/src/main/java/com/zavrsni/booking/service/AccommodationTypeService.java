package com.zavrsni.booking.service;

import com.zavrsni.booking.model.AccommodationType;

/**
 * Created by mrados on 03/02/2019
 */
public interface AccommodationTypeService {
    AccommodationType getAccommodationTypeById(Long id);

    boolean existsById(Long id);
}
