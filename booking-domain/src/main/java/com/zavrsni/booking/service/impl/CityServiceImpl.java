package com.zavrsni.booking.service.impl;

import com.zavrsni.booking.model.City;
import com.zavrsni.booking.repository.CityRepository;
import com.zavrsni.booking.service.CityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

/**
 * Created by mrados on 03/02/2019
 */
@Service
public class CityServiceImpl implements CityService {

    @Autowired
    CityRepository cityRepository;

    @Override
    public City getCityById(Long id) {
        return cityRepository.getOne(id);
    }

    @Override
    public boolean existsById(Long id) {
        return cityRepository.existsById(id);
    }
}
