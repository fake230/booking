package com.zavrsni.booking.service.impl;

import com.zavrsni.booking.model.Review;
import com.zavrsni.booking.repository.ReviewRepository;
import com.zavrsni.booking.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Created by mrados on 07.03.19.
 */
@Repository
public class ReviewServiceImpl implements ReviewService {

    @Autowired
    ReviewRepository reviewRepository;

    @Override
    public Review saveReview(Review review) {
        return reviewRepository.save(review);
    }

    @Override
    public boolean existsByAccommodationAndUser(Long accommodationId, Long userId) {
        return reviewRepository.existsByAccommodation_IdAndUserId(accommodationId, userId);
    }

    @Override
    public Review getByAccommodationAndUser(Long accommodationId, Long userId) {
        return reviewRepository.getByAccommodationIdAndUserId(accommodationId, userId);
    }

    @Override
    public Review getById(Long id) {
        return reviewRepository.getOne(id);
    }
}
