package com.zavrsni.booking.service;

import com.zavrsni.booking.model.Amenity;

/**
 * Created by mrados on 03/02/2019
 */
public interface AmenityService {
    Amenity getAmenityById(Long id);
    boolean existsById(Long id);
}
